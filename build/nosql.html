<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>NoSQL Databases &mdash; Cloud Databases 0.0.1 documentation</title>
    
    <link rel="stylesheet" href="_static/default.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '0.0.1',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="top" title="Cloud Databases 0.0.1 documentation" href="index.html" />
    <link rel="next" title="CouchDB" href="couch.html" />
    <link rel="prev" title="Introduction" href="intro.html" /> 
  </head>
  <body>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="couch.html" title="CouchDB"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="intro.html" title="Introduction"
             accesskey="P">previous</a> |</li>
        <li><a href="index.html">Cloud Databases 0.0.1 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body">
            
  <div class="section" id="nosql-databases">
<h1>NoSQL Databases<a class="headerlink" href="#nosql-databases" title="Permalink to this headline">¶</a></h1>
<p>The term &#8216;NoSQL database&#8217; is generally applied to any database that does not use the relational database model. While there are many different kinds of NoSQL databases, all of them were created for the same purpose; to face the challenges of modern computing, many of which are driven by large amounts of data and user traffic.
NoSQL databases generally are generally not designed to use relations in the same way a relational database does.</p>
<p>Some NoSQL databases, such as FountainDB, stretch the boundaries of the term &#8216;NoSQL&#8217; by allowing SQL to be used with it. This has led some to interpret &#8216;NoSQL&#8217; as an acronym for <strong>N**ot **O**nly **SQL</strong>, but these cases are not very common.</p>
<div class="section" id="the-cap-theorem">
<h2>The CAP Theorem<a class="headerlink" href="#the-cap-theorem" title="Permalink to this headline">¶</a></h2>
<p>The <strong>CAP Theorem</strong> is a name given to an ideology for designing distributed systems. It highlights a few different ways to build an application with its logic distributed over large networks. The name, CAP, is an acroynm for each of the three main concerns when designing a distributed system.</p>
<ul class="simple">
<li><strong>Consistency</strong> is the ability for users to view the same data, even if there are concurrent updates.</li>
<li><strong>Availability</strong> is the ability for all users to be able to access some version of the data.</li>
<li><strong>Partition tolerance</strong> is the ability for a database to be split over multiple servers.</li>
</ul>
<div class="figure align-center">
<img alt="The CAP Theorem. It is theoretically impossible to satisfy all three at once. It is not viable to forefit partition tolerance for a distributed system." src="_images/captheorem.jpg" />
</div>
<p>The catch is that no distributed system can satisfy all three of these concerns at the same time, one of them must be sacrified, as shown by the above diagram.
Theoretically, there are three possible design options, but sacrificing partition tolerance is not viable for a distributed system because without partition tolerance, it will be forced to give up consistency or availability during a partition. An example of a type of system that does sacrifice partition tolerance is a relational database.</p>
<p>Therefore, the real choice is whether the designer would prefer to choose to include consistency (enforced consistency), or availability (eventual consistency) into their design.</p>
<div class="section" id="enforced-consistency">
<h3>Enforced consistency<a class="headerlink" href="#enforced-consistency" title="Permalink to this headline">¶</a></h3>
<p><strong>Enforced consistency</strong> allows for the same type of consistency guarantees that are present in relational databases while still allowing for a good deal of partition tolerance.
Generally, the term enforced consistency is very broad, and can apply to many different consistency models.
FoundationDB, for example, uses <strong>sequential consistency</strong>, and is much stricter than some others.</p>
<p>NoSQL databases that implement enforced consistency include:</p>
<blockquote>
<div><ul class="simple">
<li>BigTable</li>
<li>FoundationDB</li>
<li>Hbase</li>
<li>MongoDB</li>
<li>Redis</li>
<li>Scalaris</li>
<li>Terrastore</li>
</ul>
</div></blockquote>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">MongoDB can be configured for eventual consistency.</p>
</div>
<div class="admonition warning">
<p class="first admonition-title">Warning</p>
<p class="last">Depending on the source, some NoSQL databases that are here classified as favoring enforced consistency may be grouped alongside databases that implement eventual consistency.</p>
</div>
</div>
<div class="section" id="eventual-consistency">
<h3>Eventual consistency<a class="headerlink" href="#eventual-consistency" title="Permalink to this headline">¶</a></h3>
<p><strong>Eventual consistency</strong> is a guarantee that, eventually, all users accessing the data will obtain the most recent version of the data.
While it is possible for the servers to be at odds with each other, a system designed to support eventual consistency will ensure that any alteration made to one copy of the data will
eventually be made to all copies.
However, it takes time for all of the servers to converge into the same state, and during this time, the system will not provide single-system image, but provide arbitary values instead.
In other words, if the service does not have the most recent state of the database, it will provide the most recent state possible.</p>
<p>While it seems that a system that provides eventual consistency cannot provide the same level of guarantees as a traditional ACID system, however, that is not always the case, as CouchDB does provide some basic ACID semantics.</p>
<p>NoSQL databases that implement eventual consistency include:</p>
<blockquote>
<div><ul class="simple">
<li>Cassandra</li>
<li>CouchDB</li>
<li>Dynamo</li>
<li>KAI</li>
<li>Riak</li>
<li>Tokyo Cabinet</li>
<li>Voldemort</li>
</ul>
</div></blockquote>
</div>
</div>
<div class="section" id="key-value-stores">
<h2>Key-Value stores<a class="headerlink" href="#key-value-stores" title="Permalink to this headline">¶</a></h2>
<p>Often times, NoSQL databases run on the principle of a key-value store. A key-value store is the simplest kind of NoSQL database, and is essentially a large hash table of keys and values.
Because of this, key-value stores don&#8217;t have a set schema to them, meaning that querying the entire database may not necessarily be simple, as the data is not physically grouped together.
However, data may be logically grouped together by using a bucket, a collection of key/value entries, a way of making the data more organized. In order to access a value in a bucket, you need its key, a unique ID that is required to read, update, or delete the value it is associated with.
The same key value can be used in different buckets to point to different values.</p>
<p>Two examples of popular key-value stores are Riak and Amazon&#8217;s Dynamo.</p>
</div>
<div class="section" id="document-database">
<h2>Document database<a class="headerlink" href="#document-database" title="Permalink to this headline">¶</a></h2>
<p>A document database is simiar to a key-value store in many respects.
However, in a document store, each key is associated with a data structure (known as a document) instead of one specific value. These documents can offer a little more flexibility, as they can contain many key-value pairs, or even other documents.
Unlike in a key-value store, each attribute in a document can be queried based on some of its contents, allowing the documents to mimic the functionalities of a relational database much more easily than a key-value store.</p>
<p>The documents in a document database can be formatted using existing, tried-and-true technologies. CouchDB, for example, uses JSON (JavaScript Object Notation) for its document struture, while other common formats include XML or BSON(binary encoded JSON objects).</p>
<p>The most popular document databases are CouchDB and MongoDB, both of which will be explored in greater detail later in this writing.</p>
</div>
<div class="section" id="column-store-database">
<h2>Column store database<a class="headerlink" href="#column-store-database" title="Permalink to this headline">¶</a></h2>
<p>A column store database stores data in columns instead of rows. Columns of data are grouped into column families, which can be created in the database schema or at runtime.</p>
<p>The benefit of a column based database can be seen when compared to a relational database, which is based on rows. When querying a large relational database based on one column, that may take a considerable amount of time, especially if there are thousands of rows. In a column-based database, querying every row for information in only one column is easier since only one column family would need to be queried.</p>
<p>A popular column store database is Cassandra.</p>
</div>
<div class="section" id="graph-store-database">
<h2>Graph store database<a class="headerlink" href="#graph-store-database" title="Permalink to this headline">¶</a></h2>
<p>A graph base database does not contain the usual rows and columns of a normal database. Instead, a graph store holds its data in a flexible graphical representation.</p>
<p>Each graph in a graph store database contains nodes (objects), which are connected by using edges (relationships). Both nodes and relationships can contain key-value pairs, allowing for very intricate relationships to be shown easily.</p>
<p>Popular graph store databases include Neo4J and HyperGraphDB.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">NoSQL Databases</a><ul>
<li><a class="reference internal" href="#the-cap-theorem">The CAP Theorem</a><ul>
<li><a class="reference internal" href="#enforced-consistency">Enforced consistency</a></li>
<li><a class="reference internal" href="#eventual-consistency">Eventual consistency</a></li>
</ul>
</li>
<li><a class="reference internal" href="#key-value-stores">Key-Value stores</a></li>
<li><a class="reference internal" href="#document-database">Document database</a></li>
<li><a class="reference internal" href="#column-store-database">Column store database</a></li>
<li><a class="reference internal" href="#graph-store-database">Graph store database</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="intro.html"
                        title="previous chapter">Introduction</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="couch.html"
                        title="next chapter">CouchDB</a></p>
  <h3>This Page</h3>
  <ul class="this-page-menu">
    <li><a href="_sources/nosql.txt"
           rel="nofollow">Show Source</a></li>
  </ul>
<div id="searchbox" style="display: none">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="couch.html" title="CouchDB"
             >next</a> |</li>
        <li class="right" >
          <a href="intro.html" title="Introduction"
             >previous</a> |</li>
        <li><a href="index.html">Cloud Databases 0.0.1 documentation</a> &raquo;</li> 
      </ul>
    </div>
    <div class="footer">
        &copy; Copyright 2013, Cooper Wickum.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 1.2b3.
    </div>
  </body>
</html>