/** * @author Cooper Wickum */

import java.io.IOException;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;

public class Example01_ReadandPrintOneName {

	public static void main(String args[]) {

		// Create session, select database.
		Session s = new Session("localhost", 5984);
		Database db = s.getDatabase("company");

		try {
			// Get the document ID
			Document doc = db.getDocument("Smith");

			// Use optString instead of getString so that in case the field
			// doesn't exist, you can return a default value.
			String fname = doc.optString("fname", "");
			String mname = doc.optString("mname", "");
			String mname2 = doc.optString("mname2", "");
			String lname = doc.optString("lname", "");

			// Print the string.
			System.out.print(addSpacing(fname) + addSpacing(mname)
					+ addSpacing(mname2) + addSpacing(lname));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Used for quick formatting if the String has at least 1 character in it,
	// it adds a space to the end of it. Used so "Jim Clark" is not rendered as
	// "Jim   Clark".
	
	public static String addSpacing(String str) {

		if (str.length() > 0) {
			return str + " ";
		} else
			return str;

	}

}
