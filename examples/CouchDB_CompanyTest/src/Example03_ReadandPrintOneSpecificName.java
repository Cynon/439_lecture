/** * @author Cooper Wickum */

import java.io.IOException;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;
import com.fourspaces.couchdb.ViewResults;

public class Example03_ReadandPrintOneSpecificName {

	public static void main(String args[]) {

		// Create session, select database.
		Session s = new Session("localhost", 5984);
		Database db = s.getDatabase("company");

		// Select all documents with the field fname with Jim as the contents.
		ViewResults viewRes = db.adhoc("function (doc) { if (doc.fname=='Jim') { emit(null, doc); }}");

		for (Document d : viewRes.getResults()) {

			try {
				Document doc = db.getDocument(d.getId());
				String fname = doc.optString("fname", "");
				String mname = doc.optString("mname", "");
				String mname2 = doc.optString("mname2", "");
				String lname = doc.optString("lname", "");

				System.out.print(addSpacing(fname) + addSpacing(mname)
						+ addSpacing(mname2) + addSpacing(lname) + "\n");

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	// Used for quick formatting if the String has at least 1 character in it,
	// it adds a space to the end of it. Used so "Jim Clark" is not rendered as
	// "Jim   Clark".

	public static String addSpacing(String str) {

		if (str.length() > 0) {
			return str + " ";
		} else
			return str;

	}

}
