/** * @author Cooper Wickum */

import java.io.IOException;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;

public class Example04_CreateDocument {

	public static void main(String args[]) {

		// Create session, select database.
		Session s = new Session("localhost", 5984);
		Database db = s.getDatabase("company");

		// Create the new document, assign fields.
		try {
			Document doc = new Document();
			doc.setId("Schumi");
			doc.put("fname", "Michael");
			doc.put("lname", "Schumacher");
			db.saveDocument(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
