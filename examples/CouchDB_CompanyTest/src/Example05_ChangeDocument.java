/** * @author Cooper Wickum */

import java.io.IOException;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;

public class Example05_ChangeDocument {

	public static void main(String args[]) {

		// Create session, select database.
		Session s = new Session("localhost", 5984);
		Database db = s.getDatabase("company");

		// Call the document we want to change, edit a field, save.
		try {
			Document doc = db.getDocument("Schumi");
			doc.put("fname", "Ralf");
			doc.put("mname", "Michael");
			db.saveDocument(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
