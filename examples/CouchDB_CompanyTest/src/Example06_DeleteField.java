/** * @author Cooper Wickum */

import java.io.IOException;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;

public class Example06_DeleteField {

	public static void main(String args[]) {

		// Create session, select database.
		Session s = new Session("localhost", 5984);
		Database db = s.getDatabase("company");

		// Call the document we want to change, remove a field by name, save.
		try {
			Document doc = db.getDocument("Schumi");
			doc.remove("mname");
			db.saveDocument(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
