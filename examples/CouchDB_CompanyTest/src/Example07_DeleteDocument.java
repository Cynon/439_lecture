/** * @author Cooper Wickum */

import java.io.IOException;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;

public class Example07_DeleteDocument {

	public static void main(String args[]) {

		// Create session, select database.
		Session s = new Session("localhost", 5984);
		Database db = s.getDatabase("company");

		// Call the document we want to delete, and then delete it.
		try {
			Document doc = db.getDocument("Smith");
			db.deleteDocument(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
