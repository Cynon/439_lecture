import java.net.UnknownHostException;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class Example01_GetAllItemsFromACollection {

	public static void main(String args[]) {

		try {
			MongoClient mon = new MongoClient("localhost", 27017);
			DB db = mon.getDB("company");
			DBCollection staffColl = db.getCollection("staff");

			DBCursor cursor = staffColl.find();

			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			cursor.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}