import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class Example02_GetAllItemsNamedCurran {

	public static void main(String args[]) {

		try {
			MongoClient mon = new MongoClient("localhost", 27017);
			DB db = mon.getDB("company");
			DBCollection staffColl = db.getCollection("staff");

			// Create query conditions as a BasicDBObject
			BasicDBObject query = new BasicDBObject("lname", "Curran");

			// Append another condition to return all documents with age less
			// than 30.
			query.append("age", new BasicDBObject("$lt", 30));

			DBCursor cursor = staffColl.find(query);

			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			cursor.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
