import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class Example03_CreateNewDocument {

	public static void main(String args[]) {

		try {
			MongoClient mon = new MongoClient("localhost", 27017);
			DB db = mon.getDB("company");
			DBCollection staffColl = db.getCollection("staff");

			// Create query conditions as a BasicDBObject
			BasicDBObject query = new BasicDBObject("_id", 479)
					.append("fname", "Anakin")
					.append("lname", "Skywalker")
					.append("age", 10);

			staffColl.insert(query);

			// Read all documents when we are finished so we can check the
			// output.
			DBCursor cursor = staffColl.find();

			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			cursor.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
