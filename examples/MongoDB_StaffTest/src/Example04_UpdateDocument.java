import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class Example04_UpdateDocument {

	public static void main(String args[]) {

		try {
			MongoClient mon = new MongoClient("localhost", 27017);
			DB db = mon.getDB("company");
			DBCollection staffColl = db.getCollection("staff");

			// Pick the object to update by parameters.
			BasicDBObject findQuery = new BasicDBObject("fname", "Anakin")
					.append("lname", "Skywalker");
			
			// Query for the fields to insert into update.
			BasicDBObject updateQuery = new BasicDBObject("fname", "Darth")
			.append("lname", "Vader").append("age", 35);

			// Booleans for upsert and multi
			boolean upsert = false;
			boolean multi = false;
			
			// Run the update query.
			staffColl.update(findQuery, updateQuery, upsert, multi);
			
			// Read all documents when we are finished so we can check the
			// output.
			DBCursor cursor = staffColl.find();
			
			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			cursor.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
