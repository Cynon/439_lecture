import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class Example05_SaveDocument {

	public static void main(String args[]) {

		try {
			MongoClient mon = new MongoClient("localhost", 27017);
			DB db = mon.getDB("company");
			DBCollection staffColl = db.getCollection("staff");

			// Pick the object to update by parameters.
			BasicDBObject saveNew = new BasicDBObject("fname", "Luke")
				.append("lname", "Skywalker")
				.append("age", 20);

			// Query for the fields to insert into update.
			BasicDBObject saveUpdate = new BasicDBObject("_id", "479")
				.append("fname", "James").append("mname", "Earl")
				.append("lname", "Jones").append("age", 82);

			// Run the save queries.
			staffColl.save(saveNew);
			staffColl.save(saveUpdate);

			// Read all documents when we are finished so we can check the
			// output.
			DBCursor cursor = staffColl.find();

			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			cursor.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
