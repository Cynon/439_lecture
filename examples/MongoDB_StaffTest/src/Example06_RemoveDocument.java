import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class Example06_RemoveDocument {

	public static void main(String args[]) {

		try {
			MongoClient mon = new MongoClient("localhost", 27017);
			DB db = mon.getDB("company");
			DBCollection staffColl = db.getCollection("staff");

			// Pick the object to delete by parameters.
			BasicDBObject deleteMe = new BasicDBObject("fname", "Luke")
				.append("lname", "Skywalker")
				.append("age", 20);

			// Run the remove queries.
			staffColl.remove(deleteMe);

			// Read all documents when we are finished so we can check the
			// output.
			DBCursor cursor = staffColl.find();

			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			cursor.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
